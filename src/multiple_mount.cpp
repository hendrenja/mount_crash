#include <include/multiple_mount.h>
#include <thread>

const corto_string WS_CONFIG = "ws-server.json";
const corto_string FILE_SERVER_CONFIG = "file-server.json";
const corto_string CONFIG_MOUNT = "config";
corto_string CONFIG_FILE = "config.json";
int g_updateVal = 0;

void CreateDebugMounts();
void UpdateMounts();

bool TraverseConfig()
{
    corto_iter it;

    if (corto_select("*").from(CONFIG_MOUNT).iter(&it) != 0)
    {
        corto_error("Failed to process: Error [%s]", corto_lasterr);
        return false;
    }

    while(corto_iter_hasNext(&it))
    {
        corto_result *r = (corto_result*)corto_iter_next(&it);

        corto_string nodePath = corto_asprintf("%s/%s", CONFIG_MOUNT, r->id);
        corto_object pNode = corto_lookup(root_o, nodePath);
        if (pNode == nullptr)
        {
            corto_error("Failed to find config node [%s]", nodePath);
            continue;
        }
        corto_dealloc(nodePath);

        if (corto_instanceof(multiple_mount_ToolConfig_o, pNode) == true)
        {
            // multiple_mount_ToolConfig toolConfig = (multiple_mount_ToolConfig)pNode;

            // Amphion::Config config;
            // config.ip(toolConfig->ip);
            // config.log_command(toolConfig->logCommand);
            // config.log_frequency(toolConfig->logFrequency);
            // config.log_enable(toolConfig->logEnabled);
            // config.system_frequency(toolConfig->systemFrequency);
            // config.system_enable(toolConfig->systemEnabled);
            // config.debug_frequency(toolConfig->debugFrequency);
            // config.agent_parse(toolConfig->agentParse);
            // config.benchmark(toolConfig->benchmark);
            //
            // Publish(config);
        }
        else
        {
            // corto_error("Cannot configure config node - [%s] not instance of [%s]",
            //     corto_fullpath(nullptr, pNode),
            //     corto_fullpath(nullptr, multiple_mount_ToolConfig_o));
        }

        corto_release(pNode);
    }

    return true;
}


int multiple_mountMain(int argc, char *argv[]) {

    printf("Hello Corto!\n");

    if (corto_load(CONFIG_FILE, 0, NULL) != 0)
    {
        corto_error("Failed to load [%s] - Error [%s]",
            CONFIG_FILE, corto_lasterr());
        return false;
    }

    if (corto_load(FILE_SERVER_CONFIG, 0, NULL) != 0)
    {
        corto_error("Failed to load [%s] - Error [%s]",
            FILE_SERVER_CONFIG, corto_lasterr());
        return -1;
    }


    if (corto_load(WS_CONFIG, 0, NULL) != 0)
    {
        corto_error("Failed to load [%s] - Error [%s]",
            WS_CONFIG, corto_lasterr());
        return -1;
    }

    TraverseConfig();
    CreateDebugMounts();

    while (1)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        UpdateMounts();
    }

    return 0;
}

void UpdateMounts()
{
    g_updateVal++;

    int32_t* a = (int32_t*)corto_lookup(root_o, "/debug/a");
    corto_updateBegin(a);
    corto_int32Assign(a, g_updateVal);
    corto_updateEnd(a);

    int32_t* b = (int32_t*)corto_lookup(root_o, "/debug/b");
    corto_updateBegin(b);
    corto_int32Assign(b, g_updateVal);
    corto_updateEnd(b);

    int32_t* c = (int32_t*)corto_lookup(root_o, "/debug/c");
    corto_updateBegin(c);
    corto_int32Assign(c, g_updateVal);
    corto_updateEnd(c);
    
    corto_release(a);
    corto_release(b);
    corto_release(c);
}

void CreateDebugMounts()
{
    corto_object o = corto_lookup(root_o, "debug");
    if (o == nullptr)
    {
        o = corto_createChild(root_o, "debug", corto_void_o);
    }

    corto_createChild(o, "a", corto_int32_o);
    corto_createChild(o, "b", corto_int32_o);
    corto_createChild(o, "c", corto_int32_o);
}
